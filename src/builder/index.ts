/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    NodeBlock,
    Num,
    Slots,
    affects,
    castToNumber,
    conditions,
    definition,
    each,
    editor,
    filter,
    isBoolean,
    isNumberFinite,
    isString,
    npgettext,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { ScaleOption } from "./option";
import { ScaleMatchCondition } from "./conditions/match";
import { ScaleCompareCondition } from "./conditions/compare";
import { MAX, MAX_DEFAULT, MIN, MIN_DEFAULT } from "../runner/range";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:scale", "Scale");
    },
})
export class Scale extends NodeBlock {
    @definition
    @affects("#label")
    @affects("#slots")
    mode: "numeric" | "options" = "numeric";

    @definition
    @affects("#label")
    options = Collection.of(ScaleOption, this as Scale);

    @definition
    @affects("#label")
    from?: number;

    @definition
    @affects("#label")
    to?: number;

    @definition
    labelLeft?: string;

    @definition
    labelCenter?: string;

    @definition
    labelRight?: string;

    @definition
    justify?: boolean;

    scaleSlot!: Slots.Number | Slots.String;

    get label() {
        if (this.mode === "numeric") {
            const from = Num.range(
                Num.min(
                    castToNumber(this.from, MIN_DEFAULT),
                    castToNumber(this.to, MAX_DEFAULT)
                ),
                MIN,
                MAX
            );
            const to = Num.range(
                Num.max(
                    castToNumber(this.from, MIN_DEFAULT),
                    castToNumber(this.to, MAX_DEFAULT)
                ),
                MIN,
                MAX
            );

            return `${this.type.label} (${Num.format(from)}-${Num.format(to)})`;
        } else {
            return npgettext(
                "block:scale",
                "%2 (%1 option)",
                "%2 (%1 options)",
                this.options.count,
                this.type.label
            );
        }
    }

    @slots
    defineSlots(): void {
        this.scaleSlot = this.slots.static<Slots.Number | Slots.String>({
            type: this.mode === "numeric" ? Slots.Number : Slots.String,
            reference: "scale",
            label: pgettext("block:scale", "Scale"),
            required: this.scaleSlot?.required,
            alias: this.scaleSlot?.alias,
            exportable: this.scaleSlot?.exportable,
        });
    }

    @editor
    defineEditor(): void {
        if (
            isNumberFinite(this.from) &&
            isNumberFinite(this.to) &&
            this.to < this.from
        ) {
            const from = this.from;

            this.from = this.to;
            this.to = from;
        }

        const labelLeft = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "labelLeft", undefined)
        ).placeholder(pgettext("block:scale", "Left"));
        const labelCenter = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "labelCenter", undefined)
        ).placeholder(pgettext("block:scale", "Center"));
        const labelRight = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "labelRight", undefined)
        ).placeholder(pgettext("block:scale", "Right"));

        const labelUpdate = () => {
            const count =
                this.mode === "options"
                    ? filter(this.options.all, (o) => (o.name ? true : false))
                          .length
                    : Math.abs(
                          castToNumber(this.from, MAX_DEFAULT) -
                              castToNumber(this.to, MIN_DEFAULT)
                      ) + 1;

            labelLeft.disabled(count === 0);
            labelCenter.disabled(count < 3);
            labelRight.disabled(count < 2);
        };

        this.editor.groups.general();
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:scale", "Mode"),
            form: {
                title: pgettext("block:scale", "Scale mode"),
                controls: [
                    new Forms.Radiobutton<"numeric" | "options">(
                        [
                            {
                                label: pgettext("block:scale", "Numeric scale"),
                                value: "numeric",
                            },
                            {
                                label: pgettext("block:scale", "Text scale"),
                                value: "options",
                            },
                        ],
                        Forms.Radiobutton.bind(this, "mode", "numeric")
                    ).on((t) => {
                        numbericScale.disabled(t.value === "options");
                        optionsScale.visible(t.value === "options");
                        labelUpdate();
                    }),
                ],
            },
            locked: true,
        });

        const numbericScale = this.editor.option({
            name: pgettext("block:scale", "Scale range"),
            form: {
                title: pgettext("block:scale", "Scale range"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "from", undefined, MIN_DEFAULT)
                    )
                        .label(pgettext("block:scale", "From"))
                        .min(MIN)
                        .max(MAX)
                        .on(() => labelUpdate()),
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "to", undefined, MAX_DEFAULT)
                    )
                        .label(pgettext("block:scale", "To"))
                        .min(MIN)
                        .max(MAX)
                        .on(() => labelUpdate()),
                ],
            },
            activated: true,
            locked: true,
            disabled: this.mode === "options",
        });

        const optionsScale = this.editor
            .collection({
                collection: this.options,
                title: pgettext("block:scale", "Scale options"),
                placeholder: pgettext("block:scale", "Unnamed option"),
                editable: true,
                sorting: "manual",
                onAdd: () => labelUpdate(),
                onDelete: () => labelUpdate(),
                onRename: () => labelUpdate(),
            })
            .visible(this.mode === "options");

        this.editor.option({
            name: pgettext("block:scale", "Labels"),
            form: {
                title: pgettext("block:scale", "Labels"),
                controls: [labelLeft, labelCenter, labelRight],
            },
            activated:
                isString(this.labelLeft) ||
                isString(this.labelCenter) ||
                isString(this.labelRight),
        });

        this.editor.option({
            name: pgettext("block:scale", "Width"),
            form: {
                title: pgettext("block:scale", "Width"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:scale",
                            "Justify the scale across the available width"
                        ),
                        Forms.Checkbox.bind(this, "justify", undefined, true)
                    ),
                ],
            },
            activated: isBoolean(this.justify),
        });

        this.editor.groups.options();
        this.editor.required(() => this.scaleSlot);
        this.editor.visibility();
        this.editor.alias(() => this.scaleSlot);
        this.editor.exportable(() => this.scaleSlot);
    }

    @conditions
    defineConditions(): void {
        if (this.mode === "options") {
            this.conditions.template({
                condition: ScaleMatchCondition,
                label: pgettext("block:scale", "Unanswered"),
                props: {
                    slot: this.scaleSlot,
                },
            });

            this.options.each((option: ScaleOption) => {
                if (option.name) {
                    this.conditions.template({
                        condition: ScaleMatchCondition,
                        label: option.name,
                        props: {
                            slot: this.scaleSlot,
                            option,
                        },
                    });
                }
            });
        } else {
            each(
                [
                    {
                        mode: "equal" as "equal",
                        label: pgettext("block:scale", "Answer is equal to"),
                    },
                    {
                        mode: "below" as "below",
                        label: pgettext("block:scale", "Answer is lower than"),
                    },
                    {
                        mode: "above" as "above",
                        label: pgettext("block:scale", "Answer is higher than"),
                    },
                    {
                        mode: "between" as "between",
                        label: pgettext("block:scale", "Answer is between"),
                    },
                    {
                        mode: "defined" as "defined",
                        label: pgettext("block:scale", "Answer is specified"),
                    },
                    {
                        mode: "undefined" as "undefined",
                        label: pgettext(
                            "block:scale",
                            "Answer is not specified"
                        ),
                    },
                ],
                (condition) => {
                    this.conditions.template({
                        condition: ScaleCompareCondition,
                        label: condition.label,
                        props: {
                            slot: this.scaleSlot,
                            mode: condition.mode,
                        },
                    });
                }
            );
        }
    }
}
