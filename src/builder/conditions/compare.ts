/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Num,
    affects,
    castToNumber,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { Scale } from "..";
import { TMode } from "../../runner/conditions/mode";
import { MAX, MAX_DEFAULT, MIN, MIN_DEFAULT } from "../../runner/range";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:compare`,
    version: PACKAGE_VERSION,
    context: Scale,
    icon: ICON,
    get label() {
        return pgettext("block:scale", "Compare scale answer");
    },
})
export class ScaleCompareCondition extends ConditionBlock {
    @definition
    @affects("#name")
    mode: TMode = "equal";

    @definition
    @affects("#name")
    value?: number;

    @definition
    @affects("#name")
    to?: number;

    get scale() {
        return (
            (this.node?.block instanceof Scale && this.node.block) || undefined
        );
    }

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            switch (this.mode) {
                case "between":
                    return `\`${this.slot.toString(this.value)}\` ≤ @${
                        this.slot.id
                    } ≤ \`${this.slot.toString(this.to)}\``;
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:scale",
                        "specified"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:scale",
                        "not specified"
                    )}`;
                case "above":
                case "below":
                case "equal":
                    return `@${this.slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${`\`${this.slot.toString(this.value)}\``}`;
            }
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        const from = Num.range(
            Num.min(
                castToNumber(this.scale?.from, MIN_DEFAULT),
                castToNumber(this.scale?.to, MAX_DEFAULT)
            ),
            MIN,
            MAX
        );
        const to = Num.range(
            Num.max(
                castToNumber(this.scale?.from, MIN_DEFAULT),
                castToNumber(this.scale?.to, MAX_DEFAULT)
            ),
            MIN,
            MAX
        );

        const valueTo = new Forms.Numeric(
            Forms.Numeric.bind(this, "to", undefined, 0)
        )
            .min(from)
            .max(to)
            .visible(this.mode === "between");

        const group = new Forms.Group([
            new Forms.Numeric(Forms.Numeric.bind(this, "value", undefined, 0))
                .min(from)
                .max(to)
                .autoFocus(),
            valueTo,
        ]).visible(this.mode !== "defined" && this.mode !== "undefined");

        this.editor.form({
            title: pgettext("block:scale", "When answer:"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext("block:scale", "Is equal to"),
                            value: "equal",
                        },
                        {
                            label: pgettext("block:scale", "Is lower than"),
                            value: "below",
                        },
                        {
                            label: pgettext("block:scale", "Is higher than"),
                            value: "above",
                        },
                        {
                            label: pgettext("block:scale", "Is between"),
                            value: "between",
                        },
                        {
                            label: pgettext("block:scale", "Is specified"),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:scale", "Is not specified"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    group.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    valueTo.visible(mode.value === "between");
                }),
                group,
            ],
        });
    }
}
