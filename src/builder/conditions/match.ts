/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "tripetto";
import { Scale } from "..";
import { ScaleOption } from "../option";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:match`,
    version: PACKAGE_VERSION,
    context: Scale,
    icon: ICON,
    get label() {
        return pgettext("block:scale", "Match scale answer");
    },
})
export class ScaleMatchCondition extends ConditionBlock {
    @affects("#condition")
    @definition("options")
    @affects("#name")
    option?: ScaleOption;

    get name() {
        return (
            (this.option
                ? this.option.name
                : pgettext("block:scale", "Unanswered")) || this.type.label
        );
    }
}
