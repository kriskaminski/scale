import {
    Collection,
    Forms,
    definition,
    editor,
    isString,
    name,
    pgettext,
} from "tripetto";
import { Scale } from "./";

export class ScaleOption extends Collection.Item<Scale> {
    @definition
    @name
    name = "";

    @definition
    value?: string;

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:scale", "Label"),
            form: {
                title: pgettext("block:scale", "Option label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:scale", "Identifier"),
            form: {
                title: pgettext("block:scale", "Option identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined, "")
                    ),

                    new Forms.Static(
                        pgettext(
                            "block:scale",
                            // tslint:disable-next-line:max-line-length
                            "If an option identifier is set, this identifier will be used as selected option value instead of the option label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });
    }
}
