/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    castToNumber,
    condition,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:compare`,
})
export class ScaleCompareCondition extends ConditionBlock<{
    mode: TMode;
    value?: number;
    to?: number;
}> {
    @condition
    compare(): boolean {
        const scaleSlot = this.valueOf<number>();

        if (scaleSlot) {
            switch (this.props.mode) {
                case "equal":
                    return (
                        scaleSlot.hasValue &&
                        scaleSlot.value === this.props.value
                    );
                case "below":
                    return (
                        scaleSlot.hasValue &&
                        scaleSlot.value < castToNumber(this.props.value)
                    );
                case "above":
                    return (
                        scaleSlot.hasValue &&
                        scaleSlot.value > castToNumber(this.props.value)
                    );
                case "between":
                    return (
                        scaleSlot.hasValue &&
                        scaleSlot.value >= castToNumber(this.props.value) &&
                        scaleSlot.value <= castToNumber(this.props.to)
                    );
                case "defined":
                    return scaleSlot.hasValue;
                case "undefined":
                    return !scaleSlot.hasValue;
            }
        }

        return false;
    }
}
