/** Dependencies */
import {
    NodeBlock,
    Num,
    Slots,
    assert,
    castToNumber,
} from "tripetto-runner-foundation";
import { IScale } from "./interface";
import { MAX, MAX_DEFAULT, MIN, MIN_DEFAULT } from "./range";

export abstract class Scale extends NodeBlock<IScale> {
    /** Contains the scale slot. */
    readonly scaleSlot = assert(
        this.valueOf<string | number, Slots.String | Slots.Number>("scale")
    ).confirm();

    /** Contains if the scale is required. */
    readonly required = this.scaleSlot.slot.required || false;

    get from(): number {
        return Num.range(
            Num.min(
                castToNumber(this.props.from, MIN_DEFAULT),
                castToNumber(this.props.to, MAX_DEFAULT)
            ),
            MIN,
            MAX
        );
    }

    get to(): number {
        return Num.range(
            Num.max(
                castToNumber(this.props.from, MIN_DEFAULT),
                castToNumber(this.props.to, MAX_DEFAULT)
            ),
            MIN,
            MAX
        );
    }

    get options() {
        return this.props.mode === "options"
            ? this.props.options
            : {
                  from: this.from,
                  to: this.to,
              };
    }
}
