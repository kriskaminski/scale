/** Imports */
import "./conditions/compare";
import "./conditions/match";

/** Exports */
export { Scale } from "./scale";
export * from "./interface";
